<?php

/**
 * @file
 * Install, update and uninstall functions for the Browscap Block module.
 */

/**
 * Implements hook_schema().
 */
function mobile_detect_block_schema() {
  $schema['mobile_detect_block'] = array(
    'description' => 'Sets up display criteria for blocks based on device detection using Mobile Detect.',
    'fields' => array(
      'module' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => "The block's origin module, from {block}.module.",
      ),
      'delta' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => "The block's unique delta within module, from {block}.delta.",
      ),
      'desktop_visibility' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Hide or show blocks in desktop devices. (0 = show,1 =hide)',
      ),
      'mobile_visibility' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Hide or show blocks in mobile devices. (0 = show,1 =hide)',
      ),
      'tablet_visibility' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Hide or show blocks in tablet devices. (0 = show,1 =hide)',
      ),
    ),
    'primary key' => array('module', 'delta'),
    'indexes' => array(
      'module' => array('module'),
      'delta' => array('delta'),
    ),
  );

  return $schema;
}
